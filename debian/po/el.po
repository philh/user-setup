# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# translation of el.po to
# Greek messages for debian-installer.
# Copyright (C) 2003 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
#
#
# Translations from iso-codes:
#     Translations taken from ICU SVN on 2007-09-09
#   Panayotis Pakos <aeikineton@yahoo.com>
#
# George Papamichelakis <george@step.gr>, 2004.
# Emmanuel Galatoulas <galas@tee.gr>, 2004.
# Konstantinos Margaritis <markos@debian.org>, 2004, 2006.
# Greek Translation Team <debian-l10n-greek@lists.debian.org>, 2004, 2005.
# quad-nrg.net <galaxico@quad-nrg.net>, 2005, 2006, 2007.
# quad-nrg.net <yodesy@quad-nrg.net>, 2006, 2008.
# QUAD-nrg.net <yodesy@quad-nrg.net>, 2006.
# galaxico@quad-nrg.net <galaxico@quad-nrg.net>, 2009, 2011.
# Emmanuel Galatoulas <galaxico@quad-nrg.net>, 2009, 2010, 2013, 2014, 2018, 2020, 2023.
#   Tobias Quathamer <toddy@debian.org>, 2007.
#   Free Software Foundation, Inc., 2004.
#   Alastair McKinstry <mckinstry@computer.org>, 2001.
#   QUAD-nrg.net <yodesy@quad-nrg.net>, 2006, 2010.
#   Simos Xenitellis <simos@hellug.gr>, 2001.
#   Konstantinos Margaritis <markos@debian.org>, 2004.
#   Athanasios Lefteris <alefteris@gmail.com>, 2008, 2012.
#   root <galatoulas@cti.gr>, 2020.
#   Vangelis Skarmoutsos <skarmoutsosv@gmail.com>, 2020.
msgid ""
msgstr ""
"Project-Id-Version: el\n"
"Report-Msgid-Bugs-To: user-setup@packages.debian.org\n"
"POT-Creation-Date: 2024-04-06 20:02+0000\n"
"PO-Revision-Date: 2023-05-13 02:46+0300\n"
"Last-Translator: galaxico <galas@tee.gr>\n"
"Language-Team: Greek <debian-l10n-greek@lists.debian.org>\n"
"Language: el\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#. Type: boolean
#. Description
#. :sl1:
#: ../user-setup-udeb.templates:5001
msgid "Allow login as root?"
msgstr "Να επιτρέπεται η είσοδος σαν χρήστης root;"

#. Type: boolean
#. Description
#. :sl1:
#: ../user-setup-udeb.templates:5001
msgid ""
"If you choose not to allow root to log in, then a user account will be "
"created and given the power to become root using the 'sudo' command."
msgstr ""
"Αν επιλέξετε να μην επιτρέψεται η είσοδος στο σύστημα σαν χρήστης root, θα "
"δημιουργηθεί ένας άλλος λογαριασμός χρήστη με την δυνατότητα να γίνεται "
"χρήστης root χρησιμοποιώντας την εντολή 'sudo'."

#. Type: password
#. Description
#. :sl1:
#: ../user-setup-udeb.templates:6001
msgid "Root password:"
msgstr "Κωδικός του χρήστη root:"

#. Type: password
#. Description
#. :sl1:
#: ../user-setup-udeb.templates:6001
msgid ""
"Some account needs to be available with administrative super-user "
"privileges. The password for that account should be something that cannot be "
"guessed."
msgstr ""

#. Type: password
#. Description
#. :sl1:
#: ../user-setup-udeb.templates:6001
msgid ""
"To allow direct password-based access via the 'root' account, you can set "
"the password for that account here."
msgstr ""

#. Type: password
#. Description
#. :sl1:
#: ../user-setup-udeb.templates:6001
msgid ""
"Alternatively, you can lock the root account's password by leaving this "
"setting empty, and instead use the system's initial user account (which will "
"be set up in the next step) to gain administrative privileges. This will be "
"enabled for you by adding that initial user to the 'sudo' group."
msgstr ""

#. Type: password
#. Description
#. :sl1:
#: ../user-setup-udeb.templates:6001
msgid "Note: what you type here will be hidden (unless you select to show it)."
msgstr ""

#. Type: password
#. Description
#. :sl1:
#. Type: password
#. Description
#. :sl1:
#: ../user-setup-udeb.templates:7001 ../user-setup-udeb.templates:14001
msgid "Re-enter password to verify:"
msgstr "Ξαναδώστε τον κωδικό για επιβεβαίωση:"

#. Type: password
#. Description
#. :sl1:
#: ../user-setup-udeb.templates:7001
msgid ""
"Please enter the same root password again to verify that you have typed it "
"correctly."
msgstr ""
"Παρακαλώ ξαναδώστε τον ίδιο κωδικό του root για την εξακρίβωση της ορθής "
"πληκτρολογησής του."

#. Type: boolean
#. Description
#. :sl2:
#: ../user-setup-udeb.templates:8001
msgid "Create a normal user account now?"
msgstr "Θέλετε να δημιουργήσετε έναν απλό χρήστη τώρα;"

#. Type: boolean
#. Description
#. :sl2:
#: ../user-setup-udeb.templates:8001
msgid ""
"It's a bad idea to use the root account for normal day-to-day activities, "
"such as the reading of electronic mail, because even a small mistake can "
"result in disaster. You should create a normal user account to use for those "
"day-to-day tasks."
msgstr ""
"Δε συνιστάται να χρησιμοποιείτε τον χρήστη root για καθημερινές εργασίες, "
"όπως ανάγνωση της αλληλογραφίας, γιατί ακόμη και ένα μικρό λάθος μπορεί να "
"αποβεί καταστροφικό. Τώρα, μπορείτε να δημιουργήσετε έναν απλό χρήστη για "
"τις καθημερινές εργασίες."

#. Type: boolean
#. Description
#. :sl2:
#: ../user-setup-udeb.templates:8001
#, fuzzy
msgid ""
"Note that you may create it later (as well as any additional account) by "
"typing 'adduser <username>' as root, where <username> is a username, like "
"'imurdock' or 'rms'."
msgstr ""
"Σημειώστε ότι μπορείτε αν θέλετε να δημιουργήσετε το λογαριασμό (ή και "
"περισσότερους αν επιθυμείτε) με την εκτέλεση της εντολής 'adduser "
"<username>' ως χρήστης root, όπου <username> είναι το όνομα χρήστη όπως "
"'imurdock' ή 'rms'."

#. Type: string
#. Description
#. :sl1:
#: ../user-setup-udeb.templates:9001
msgid "Full name for the new user:"
msgstr "Το πλήρες όνομα του νέου χρήστη:"

#. Type: string
#. Description
#. :sl1:
#: ../user-setup-udeb.templates:9001
msgid ""
"A user account will be created for you to use instead of the root account "
"for non-administrative activities."
msgstr ""
"Θα δημιουργηθεί ένας λογαριασμός απλού χρήστη, τον οποίο συνιστάται να "
"χρησιμοποιείτε για τις καθημερινές μη διαχειριστικές εργασίες αντί του "
"λογαριασμού root."

#. Type: string
#. Description
#. :sl1:
#: ../user-setup-udeb.templates:9001
msgid ""
"Please enter the real name of this user. This information will be used for "
"instance as default origin for emails sent by this user as well as any "
"program which displays or uses the user's real name. Your full name is a "
"reasonable choice."
msgstr ""
"Παρακαλώ εισάγετε το πραγματικό όνομα αυτού του χρήστη. Αυτή η πληροφορία θα "
"χρησιμοποιηθεί, για παράδειγμα, σαν προκαθορισμένη αφετηρία στα μηνύματα "
"email που θα αποστέλλονται από τον χρήστη αυτό καθώς και από οποιοδήποτε "
"πρόγραμμα εμφανίζει ή χρησιμοποιεί το πραγματικό όνομα του χρήστη. Το πλήρες "
"όνομά σας είναι μια εύλογη επιλογή."

#. Type: string
#. Description
#. :sl1:
#: ../user-setup-udeb.templates:10001
msgid "Username for your account:"
msgstr "Εισάγετε το όνομα χρήστη του λογαριασμού σας:"

#. Type: string
#. Description
#. :sl1:
#: ../user-setup-udeb.templates:10001
msgid ""
"Select a username for the new account. Your first name is a reasonable "
"choice. The username should start with a lower-case letter, which can be "
"followed by any combination of numbers and more lower-case letters."
msgstr ""
"Επιλέξτε ένα ονομα χρήστη για το νέο λογαριασμό. Το μικρό σας όνομα είναι "
"μια λογική επιλογή. Το όνομα χρήστη θα πρέπει να ξεκινά με ένα πεζό γράμμα "
"και να ακολουθείται απο οποιονδήποτε συνδυασμό αριθμών και πεζών γραμμάτων."

#. Type: error
#. Description
#. :sl2:
#: ../user-setup-udeb.templates:11001
msgid "Invalid username"
msgstr "Μη έγκυρο όνομα χρήστη"

#. Type: error
#. Description
#. :sl2:
#: ../user-setup-udeb.templates:11001
msgid ""
"The username you entered is invalid. Note that usernames must start with a "
"lower-case letter, which can be followed by any combination of numbers and "
"more lower-case letters, and must be no more than 32 characters long."
msgstr ""
"Το όνομα χρήστη που δώσατε δεν είναι έγκυρο. Σημειώστε ότι το όνομα χρήστη "
"θα πρέπει να αρχίζει με πεζό γράμμα πού μπορεί να ακολουθείται από "
"οποιονδήποτε συνδυασμό αριθμών και πεζών γραμμάτων ενώ τό μήκος του δεν θα "
"πρέπει νά υπερβαίνει τους 32 χαρακτήρες."

#. Type: error
#. Description
#. :sl2:
#: ../user-setup-udeb.templates:12001
msgid "Reserved username"
msgstr "Δεσμευμένο όνομα χρήστη"

#. Type: error
#. Description
#. :sl2:
#: ../user-setup-udeb.templates:12001
msgid ""
"The username you entered (${USERNAME}) is reserved for use by the system. "
"Please select a different one."
msgstr ""
"Το όνομα χρήστη που εισάγατε (${USERNAME}) είναι δεσμευμένο για χρήση από το "
"σύστημα. Παρακαλώ διαλέξτε ένα διαφορετικό."

#. Type: password
#. Description
#. :sl1:
#: ../user-setup-udeb.templates:13001
msgid "Choose a password for the new user:"
msgstr "Διαλέξτε έναν κωδικό πρόσβασης για το νέο χρήστη:"

#. Type: password
#. Description
#. :sl1:
#: ../user-setup-udeb.templates:13001
msgid "Make sure to select a strong password that cannot be guessed."
msgstr ""

#. Type: password
#. Description
#. :sl1:
#: ../user-setup-udeb.templates:14001
msgid ""
"Please enter the same user password again to verify you have typed it "
"correctly."
msgstr "Παρακαλώ ξαναδώστε τον ίδιο κωδικό για την επιβεβαίωσή του."

#. Type: error
#. Description
#. :sl2:
#: ../user-setup-udeb.templates:15001
msgid "Password input error"
msgstr "Σφάλμα κατά την εισαγωγή του κωδικού"

#. Type: error
#. Description
#. :sl2:
#: ../user-setup-udeb.templates:15001
msgid "The two passwords you entered were not the same. Please try again."
msgstr "Οι δύο κωδικοί που δώσατε διαφέρουν. Παρακαλώ ξαναπροσπαθήστε."

#. Type: error
#. Description
#. :sl2:
#: ../user-setup-udeb.templates:16001
msgid "Empty password"
msgstr "Κενός κωδικός πρόσβασης"

#. Type: error
#. Description
#. :sl2:
#: ../user-setup-udeb.templates:16001
msgid ""
"You entered an empty password, which is not allowed. Please choose a non-"
"empty password."
msgstr ""
"Εισάγατε έναν κενό κωδικό πρόσβασης, που δεν είναι επιτρεπτός. Παρακαλώ "
"διαλέξτε έναν μη κενό κωδικό πρόσβασης."

#. Type: title
#. Description
#. :sl1:
#: ../user-setup-udeb.templates:17001
msgid "Set up users and passwords"
msgstr "Ρύθμιση χρηστών και κωδικών πρόσβασης"

#. Type: text
#. Description
#. finish-install progress bar item
#. :sl1:
#: ../user-setup-udeb.templates:18001
msgid "Setting users and passwords..."
msgstr "Ρύθμιση χρηστών και κωδικών πρόσβασης..."
